---
title: "Optum KM Curves"
author: "Vidhika Tiwari"
date: "5/13/2021"
output:
  word_document: default
  html_document: default
---


## Import Required Packages and datasets

```{r setup, include=FALSE}
require(survival)
require(survminer)
require(dplyr)
require(data.table)
require(rio)
require(corrplot)
require(Boruta)
require(mltools)
require(purrr)
library(survRM2)
library(ranger)
library(ggplot2)
library(dplyr)
library(ggfortify)
require(knitr)


optum <- read.csv("Data/Optum Significant Features.csv")

optum = optum %>% mutate(age = ifelse(age>65, 0, 1))
optum = optum %>% mutate(gdr_cd = ifelse(gdr_cd == 'M', 0, 1))
optum = optum %>% mutate(bus = ifelse(bus == 'COM', 1, 0))

```

##Overall persistence curve for Optum data 

```{r fig.height=8, fig.width=8}

surv_object <- Surv(time = optum$episode_length, event = optum$drop_off)

fit1 <- survfit(surv_object ~ 1, data = optum)
  
  print(summary(fit1, times = c(1,30,60,90*(1:10))))
  print(ggsurvplot(fit1, data = optum, pval = TRUE,  palette = "simpsons",conf.int = TRUE, risk.table = TRUE, tables.height = 0.20))
 
```

##Getting KM curves for categorical features

The KM curves from Optum covariates are being used for benchmarking and comparison with PSP. 

```{r fig.height=8, fig.width=8}

#defining the survival object
surv_object <- Surv(time = optum$episode_length, event = optum$drop_off)

#looping through all columns 
cols = colnames(optum[4:44])

for (i in 1:length(cols)) {
  col = cols[i]
  
  #fitting the data to survival object
  var = data.frame(optum[[col]])
  colnames(var) <- c('value')
  var = var %>% mutate(value = ifelse(value > 0, 1, 0))
  fit1 <- survfit(surv_object ~ value, data = var)
  
  #displaying the survival curve and summary table 
  print(col)
  print(summary(fit1, times = c(1,30,60,90*(1:10))))
  print(ggsurvplot(fit1, data = var, pval = TRUE, title = col,  palette = "simpsons",conf.int = TRUE, risk.table = TRUE, tables.height = 0.20))
 
  
}
```
